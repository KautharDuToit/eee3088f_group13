## Contributing to group 13 Audio Visualiser HAT (Hardware attached on Top)

Welcome and thank you for considering contributing our audio visualiser HAT. 
Please read and follow these guidelines to make the contribution process as smooth and easy as possible for everyone involved. In addition, it indicates that you agree to respect the time of developers managing and developing this open source project. Likewise, we will respond to your issues by evaluating any changes, as well as helping you finalize your pull requests.

Contributing to this repository requires that you first discuss changes with the owners via issue, email, or any other method.

Please note that the project has a code of conduct, so keep it in mind for all interactions.

## Pull Request Process

1. Update the README.md with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.
2. Increase the version numbers in any examples files and the README.md to the new version that this Pull Request would represent.
3. As soon as two other developers have signed off on the Pull Request, you can merge it, or if you do not have permission to do so, you may ask a second reviewer to merge it for you.

In general, we follow the "fork-and-pull" Git workflow

1. Fork the repository to your own Github account
2. Clone the project to your machine
3. Create a local branch with a descriptive name that is succinct
4. Commit changes to the branch following any formatting and testing guidelines specific to this repo
5. Push changes to your fork
6. Open a PR in our repository and follow the PR template so that we can efficiently review the changes.

## Code of conduct
## Our Pledge
In order to foster an open and welcoming environment, we as contributors and maintainers pledge to ensure that participation in our project and our community is free of harassment regardless of age, body size, disability, ethnicity, gender identity and expression, level of experience, nationality, appearance, race, religion, or sexual orientation.
## Our standards
Among the behaviors that contribute to creating a positive environment are:

1. Using welcoming and inclusive language
2. Being respectful of differing viewpoints and experiences
3. Gracefully accepting constructive criticism
4. Focusing on what is best for the community
5. Showing empathy towards other community members

Among the unacceptable behaviors of participants are:

1. Sexualized language and imagery as well as unwanted sexual attention and advances
2. Trolling, insults, and personal or political attacks
3. Public or private harassment
4. Publicly disclosing others' private information, like a physical or electronic address, without their consent
5. Conduct that might be deemed inappropriate in a professional setting

## Our Responsibilities
It is the responsibility of the project maintainers to clarify the standards of acceptable behavior, and they are expected to take fair and appropriate corrective action if they observe any unacceptable behaviors.

In addition, maintainers have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned with this Code of Conduct, or to ban temporarily or permanently any contributor for other behaviors that they deem inappropriate, threatening, offensive, or harmful.
## Issues
Using issues, you can report any problems with the library, request a new feature, or discuss proposed changes before creating a PR. In order to assist you with collecting and providing the information we need to investigate, a template will be loaded when you create a new issue.

In the event you find an existing issue that is related to your problem, please add your own reproduction information to the existing issue instead of creating a new one. The addition of a reaction can also help our maintainers to be aware that a particular issue affects more than just one reporter.
## Scope
When an individual is representing the project or its community, this Code of Conduct applies both within project spaces and in public spaces. Using an official project e-mail address, publishing via an official social media account, or serving as an appointed representative at an online or offline event are all examples of representing a project or community. Project maintainers can further define and clarify a project's representation.
## Enforcement
Contact the project team at AudioVisualizer@gmail.com to report instances of abusive, harassing, or otherwise objectionable behavior. All complaints will be evaluated and a response will be given if it is thought necessary and suitable under the circumstances. The project team is required to keep the identity of  reported private. 

Project maintainers who do not observe or enforce the Code of Conduct in a fair and reasonable manner may face temporary or permanent repercussions as assessed by the project's leadership.
## Getting help 
Any queries can be emailed to : AudioVisualizer@gmail.com

For additional support, join the whatsapp or discord group: https://chat.whatsapp.com/GxBIlgY3kXm8WVzX12LlB3 , https://discord.gg/Zqw7c6Du 

You could also use this link that have answers to some common questions: https://matias.ma/nsfw/
