## Audio Visualiser HAT (Hardware attached on Top)

## Description
The Audio Visualizer HAT's function is to take in an audio input through a left and right channel, split the audio into 7 different frequencies and display the music through 14 LED’s (7 For each channel). Each LED represents a frequency contained in the audio at that point in time. This will result in a constant stream of changing LED’s that will light up in sync with the music or audio. 

Links to datasheets of ICs/components used:

[Schottky Diode](https://datasheet.lcsc.com/lcsc/1809140216_Changjiang-Electronics-Tech--CJ-B5819W-SL_C8598.pdf);
[Seven Band graphic equaliser-MSGEQ7](https://www.sparkfun.com/datasheets/Components/General/MSGEQ7.pdf);
[Battery charger IC-STC4054GR](https://datasheet.lcsc.com/lcsc/1808082051_STMicroelectronics-STC4054GR_C262930.pdf);
[Voltage Regulator IC - LM1117DT-3.3](https://www.ti.com/cn/lit/ds/symlink/lm1117.pdf?ts=1652431427896);
[EEPROM chip or memory -AT24C256C-SSHL-T](https://datasheet.lcsc.com/lcsc/1809151932_Microchip-Tech-AT24C256C-SSHL-T_C6482.pdf);
[MOSFET for UVLO circuit-IRF4905](https://datasheet.lcsc.com/lcsc/1809200113_Infineon-Technologies-IRF4905STRLPBF_C2620.pdf);
[LDR-TL431D](https://datasheet.lcsc.com/lcsc/1809042020_Senba-Sensing-Tech-GL5516_C125626.pdf); 
[Reference voltage diode-CH340T](https://datasheet.lcsc.com/lcsc/2112011530_JSMSEMI-TL431_C963380.pdf)

All the components needed to build this PCB is listed in the BOM and can be found and ordered on the JLCPCB website 

## Hardware

The user would need hardware to use the PCB, the hardware needed is specified below:

- A Laptop
- STM32F051R8T6 discovery board
- Audio Visualizer PCB
- AUX cable for a 3.55mm audio jack
- Type A USB cable for the PCB
- Cable to connect discovery board

## Software

STM32CubeIDE and PuTTY configuration software are needed and can be obtained from :

- https://www.st.com/en/development-tools/stm32cubeide.html#st-get-software
- https://www.putty.org/

A CH340T driver will also be needed and can be obtained from the link below or by simply searching for the specific driver:

- https://sparks.gogo.co.nz/ch340.html

These applications alongside the code provided can be used to operate the PCB. The code allows the user to connect the USB to UART and read and write to the EEPROM chip. Code has also been provided to operate the digital sensor (the MSGEQ7 chip) to allow the audio signal to be analyzed and the output to be displayed on the LEDs. 


## Usage

Connecting the hardware

The hardware can be connected by fitting the Audio Visualizer HAT ontop of the STM32 discovery board. The PCB should now 'sit' ontop of the discovery board. The PCB can be connected to the laptop via the type A cable plugged into one of the usb ports. The discovery board can be connected to the laptop via usb port and cable as well. The AUX cable can be plugged nto a device that will provide the audio signal and can then be plugged into the audio jack on the PCB. 

Using the software and running a starting program

The code can be found in the gitrepo and downloaded. The code provided can then be opened in STM32CubeIDE. The PCB should be connected to the STM32 microcontroller and both the STM32 and the Audio Visualizer PCB should be connected to a laptop via separate USB cables. The appropriate ports should be configured via PuTTY and the code should then be built and ‘run’ on the PCB and STM32. The ‘hello-world’ program should be displayed as a message that says "Hello, this is STMF0 Discovery board: " and a blinking blue LED on the STM32 board. 

## Support

Any queries can be emailed to : AudioVisualizer@gmail.com

For additional support, join the whatsapp group: https://chat.whatsapp.com/GxBIlgY3kXm8WVzX12LlB3

## Contributing

Any information on how to contributing back to the project can be found in the CONTRBUTING file in git repo.

## Authors and acknowledgment

A special acknowledgment to the creators of the Audio Visualizer HAT. 

- Josh Harford under the sensing submodule.
- Rasheeda Taliep under the power submodule.
- Kauthar Du Toit under the microcontroller interfacing submodule.

## License
Creative Commons Attribution 4.0 International license
.
