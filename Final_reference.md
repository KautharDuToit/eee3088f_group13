Code Reference 

HAL_UART_Transmit(UART_HandleTypeDef * huart, uint8_t * pData, uint16_t Size, uint32_t Timeout);

This function sends an array of characters over the UART.

Parameters: 
huart = UART handle 
pData = Pointer to data buffer
Size = Amount of data to be sent 
Timeout = Timeout duration
	
Return Value: HAL = status

Remarks: N/A

-debugPrintln(&huart2, "Hello, this is STMF0 Discovery board");

This function prints the line in the brackets.

Parameters: &huart2

Return Value: Hello, this is STMF0 Discovery board

Remarks: N/A

-HAL_I2C_IsDeviceReady(&hi2c1, uint16_t DevAddress, uint32_t Trials, uint32_t Timeout);

This function is usedd to check if the correct address is present for the eeprom.

Parameters: 
hi2c = contains configuration information for the specified I2C. 
DevAddress = Target device address. 
Trials = Number of trails. 
Timeout = Timout duration.

Return Value: HAL = status

Remarks: Used with memory devices.

-memset(str, 0, size_t n); 

This function resets str to zeros.

Parameters: n = number of bytes to be set to the value

Return Value: Returns a pointer to the memory area str.

Remarks: N/A

-HAL_I2C_Mem_Write(&hi2c1 , uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t * pData, uint16_t Size, uint32_t Timeout);

	This function writes data to a specific memory address.

Parameters: 
DevAddress = Target device address.
MemAdress = Internal memory address. 
MemAddSize = Size of internal memory address. 
pData = Pointer to data buffer. 
Size = Amount of data to be sent. 
Timeout = Timeout duration.

Return Value: HAL_I2C_Mem_Read

Remarks: N/A

-HAL_Delay(x);

	This function implements a delay.

Parameters: x = millisecond of delay

Return Value: N/A
 
Remarks: provides an accurate delay called from peripheral ISR

-HAL_I2C_Mem_Read(&hi2c1, uint16_t DevAddress, uint16_t MemAddress, uint16_t MemAddSize, uint8_t * pData, uint16_t Size, uint32_t Timeout)

	This function reads data from a specific memory address. 

Parameters: 
DevAddress = Target device address. 
MemAddress = Internal memory address. 
MemAddSize = Size of internal memory address. 
pData = Pointer to data buffer. 
Size = Amount of data to be sent. 
Timeout = Timeout duration. 

Return Value: HAL_I2C_IsDeviceReady

Remarks: N/A

-HAL_GPIO_TogglePin(GPIOC, uint16_t GPIO_Pin);

	This function toggles the pin specified.

Parameters: GPIO_Pin = which pin is toggles 

Return Value: N/A
 
Remarks: N/A

-void loop() 
Display values from left and right channel on monitor for (int i=0; i <7; i++)
Parameters: None 
Return value: A string value representing the value of each frequency contained in the audio	signal	 
Remarks: Code loops continuously

- void readMSGEQ7s()
	Read seven spectrum bands from MSGEQ7 chips
Parameters: None 
Return Value: None 
Remarks: sets the STROBE and RESET pins and runs a for loop that reads the analog values from the MSGEQ7 chips for 7 cycles, one for each frequency channel.

- void writeLEDs()
Write 14 spectrum bands to 2x7 banks of LED’s 
Parameters: None 
Return Value: None 
Remarks: Writing the value of
